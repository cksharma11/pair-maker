const question = require("readline-sync").question;

const makePair = function(){
    const numbers = [...Array(34).keys()];
    
    while(numbers.length > 0){
        const pair = [];
        const randomOne = Math.floor(Math.random()* numbers.length);

        pair.push(numbers[randomOne]+1);
        numbers.splice(randomOne,1);

        const randomTwo = Math.floor(Math.random()* numbers.length);
        pair.push(numbers[randomTwo]+1);
        numbers.splice(randomTwo,1);
    
        console.log(pair);
        question("Press Enter to See the next pair");
    }
}

makePair();